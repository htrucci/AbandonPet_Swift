//
//  FourthViewController.swift
//  AbandonPet
//
//  Created by Htrucci on 2017. 9. 17..
//  Copyright © 2017년 Htrucci. All rights reserved.
//


import UIKit


class MyWebViewController: UIWebView, UIWebViewDelegate {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        SetMyDelegate()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        SetMyDelegate()
    }
    
    func SetMyDelegate(){
        self.delegate = self
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if(request.url?.scheme?.localizedLowercase == "abandonpet"){
            //print(request.url)
            //print(request.url?.query)
            
            if(request.url?.host?.localizedLowercase == "alert"){
                var msg = request.url?.queryItems?["abc"]
                //print(request.url?.queryItems?["abc"])
                if(msg != nil){
                    webView.stringByEvaluatingJavaScript(from: "alert('"+msg!+"')")
                }
                
                
            }
        }
        return true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
}
extension URL {
    var queryItems: [String: String]? {
        var params = [String: String]()
        return NSURLComponents(url: self as URL, resolvingAgainstBaseURL: false)?
            .queryItems?
            .reduce([:], { (_, item) -> [String: String] in
                params[item.name] = item.value
                return params
            })
    }
}
