//
//  FirstViewController.swift
//  AbandonPet
//
//  Created by Htrucci on 2017. 7. 10..
//  Copyright © 2017년 Htrucci. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SWXMLHash
import EasyToast
import DatePickerDialog

class FirstViewController: UIViewController {
    
    
    var sidoListDic = Dictionary<String, String>()
    var siguListDic = Dictionary<String, String>()
    var petKindListDic = Dictionary<String, String>()
    var selectedOrgCd: String = ""
    var selectedUprCd: String = ""
    var selectedUpkind: String = ""
    var selectedKind: String = ""
    var selectedSrchStartDy: String = ""
    var selectedSrchEndDy: String = ""
    var abandonpetinfos: [AbandonPetInfo] = []
    
    @IBOutlet weak var lblPetDetailTypeSelector: UILabel!
    @IBOutlet weak var lblPetTypeSelector: UILabel!
    @IBOutlet weak var lblGuSelector: UILabel!
    @IBOutlet weak var lblCitySelector: UILabel!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnSrchStartDy: UIButton!
    @IBOutlet weak var btnSrchEndDy: UIButton!
    @IBOutlet weak var btnPetDetailTypeSelector: UIButton!
    @IBOutlet weak var btnPetTypeSelector: UIButton!
    @IBOutlet weak var btnCitySelector: UIButton!
    @IBOutlet weak var btnGuSelector: UIButton!
    @IBOutlet weak var srchEndDy: UILabel!
    @IBOutlet weak var srchStartDy: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnSidoRemove: UIButton!
    @IBOutlet weak var btnGuRemove: UIButton!
    @IBOutlet weak var btnKindRemove: UIButton!
    @IBOutlet weak var btnDetailKindRemove: UIButton!
    @IBOutlet weak var indicatorTableView: UIActivityIndicatorView!
    @IBOutlet weak var btnSrchStartDtRemove: UIButton!
    @IBOutlet weak var btnSrchEndDtRemove: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        var request = URLRequest(url: URL(string: "https://127.0.0.1:8080/abandonpet/allCity")!)
//        request.httpMethod = "POST"
//        let session = URLSession.shared

        // Do any additional setup after loading the view, typically from a nib.
        let tap = UITapGestureRecognizer(target: self, action: #selector(FirstViewController.startDatePickerTapped(_:)))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(FirstViewController.endDatePickerTapped(_:)))
        srchStartDy.isUserInteractionEnabled = true
        srchStartDy.addGestureRecognizer(tap)
        srchEndDy.isUserInteractionEnabled = true
        srchEndDy.addGestureRecognizer(tap2)
        tableView.dataSource = self
        tableView.delegate = self
        self.indicatorTableView.stopAnimating()
        DispatchQueue.main.async{
            self.tableView.reloadData()
        }
        //self.navigationItem.prompt = "aaa"
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SidoPicker(_ sender: UIButton) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let uprCd = lblCitySelector.text!
        print(uprCd)
        let url = URL(string: "http://127.0.0.1:8080/abandonpet/allCityList")!
        let task = session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                UIUtils.popupToast(message: "네트워크 장애가 발생하였습니다", view: self.view)
            } else {
                let dataString =  String(data: data!, encoding: String.Encoding.utf8)
                //print(dataString!)
                let json = try!JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [[String:Any]]
                //siguListArray = json
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    for e in parseJSON{
                        if let orgdownNm = e["orgdownNm"] as? String{
                            let orgCd = e["orgCd"] as? String!
                            self.sidoListDic[orgCd!] = orgdownNm
                            //print(orgdownNm)
                        }
                        
                    }
                    DispatchQueue.main.async(execute: {
                        // perform on main
                        let values = Array(self.sidoListDic.values)
                        print(values)
                        ActionSheetStringPicker.show(withTitle: "시도 선택", rows:
                            values
                            , initialSelection: 0, doneBlock: {
                                picker, index, value in
                                
                                print("values = \(value!)")
                                print("indexes = \(index)")
                                print("picker = \(picker)")
                                //sender.titleLabel = value as? String
                                sender.setTitle(value as? String, for: UIControlState.normal)
                                var uprCd = ""
                                for e in self.sidoListDic{
                                    if (self.btnCitySelector.currentTitle!.isEqual(e.value)) {
                                        uprCd = e.key
                                        self.selectedUprCd = uprCd
                                    }
                                }
                                self.selectedOrgCd = ""
                                self.btnGuSelector.setTitle("구 선택 (전체)", for: UIControlState.normal)
                                print(self.selectedUprCd)
                                self.btnSidoRemove.isEnabled = true
                                return
                        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
                    });
                }
            }
        }
        task.resume()
    }
    @IBAction func GuPicker(_ sender: UIButton) {
        
        
        if self.btnCitySelector.currentTitle == "시도 선택 (전체)" {
            self.view.showToast("시도를 먼저 선택해주세요", position: .bottom, popTime: 3, dismissOnTap: false)
            return
        }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
//        var uprCd = ""
//        for e in sidoListDic{
//            if (self.btnCitySelector.currentTitle!.isEqual(e.value)) {
//                uprCd = e.key
//                self.selectedUprCd = uprCd
//            }
//        }
//        print(uprCd)
        let url = URL(string: "http://127.0.0.1:8080/abandonpet/allSiguList?uprCd="+self.selectedUprCd)!
        let task = session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                UIUtils.popupToast(message: "네트워크 장애가 발생하였습니다", view: self.view)
            } else {
                let dataString =  String(data: data!, encoding: String.Encoding.utf8)
                //print(dataString!)
                let json = try!JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [[String:Any]]
                //siguListArray = json
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    for e in parseJSON{
                        if let orgCd = e["orgCd"] as? String{
                            let orgdownNm = e["orgdownNm"] as? String!
                            self.siguListDic[orgCd] = orgdownNm
                            //print(orgdownNm)
                        }
                    }
                    DispatchQueue.main.async(execute: {
                        // perform on main
                        print(self.siguListDic.count);
                        let values = Array(self.siguListDic.values)
                        ActionSheetStringPicker.show(withTitle: "구 선택", rows:
                            values
                            , initialSelection: 0, doneBlock: {
                                picker, index, value in
                                
                                print("values = \(value.debugDescription)")
                                print("indexes = \(index)")
                                print("picker = \(picker.debugDescription)")
                                //self.btnGuSelector.text = value as? String
                                sender.setTitle(value as? String, for: UIControlState.normal)
                                for e in self.siguListDic{
                                    if (sender.currentTitle!.isEqual(e.value)) {
                                        self.selectedOrgCd = e.key
                                    }
                                }
                                self.btnGuRemove.isEnabled = true
                                return
                        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
                    });
                }
            }
        }
        task.resume()
        

        
    }
    @IBAction func petTypePicker(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "축종 선택", rows:
            ["개","고양이","기타"]
            , initialSelection: 0, doneBlock: {
                picker, index, value in
                
                print("values = \(value as! String)")
                print("indexes = \(index)")
                print("picker = \(picker!)")
                //self.lblPetType.text = value as! String
                sender.setTitle(value as? String, for: UIControlState.normal)
                if(value as! String == "개"){
                    self.selectedUpkind = "417000"
                }else if(value as! String == "고양이"){
                    self.selectedUpkind = "422400"
                }else if(value as! String == "기타"){
                    self.selectedUpkind = "429900"
                }
                self.btnKindRemove.isEnabled = true
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    @IBAction func petDetailTypePicker(_ sender: UIButton) {
        
        
        if self.btnPetTypeSelector.currentTitle == "축종 (전체)"  {
            self.view.showToast("축종을 먼저 선택해주세요", position: .bottom, popTime: 3, dismissOnTap: false)
            return
        }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        let url = URL(string: "http://127.0.0.1:8080/abandonpet/getPetKind?upKindCd="+self.selectedUpkind)!
        let task = session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                UIUtils.popupToast(message: "네트워크 장애가 발생하였습니다", view: self.view)
            } else {
                let dataString =  String(data: data!, encoding: String.Encoding.utf8)
                //print(dataString!)
                let json = try!JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [[String:Any]]
                //siguListArray = json
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    for e in parseJSON{
                        if let kindCd = e["kindCd"] as? String{
                            let kindNm = e["kindNm"] as? String!
                            self.petKindListDic[kindCd] = kindNm
                            //print(orgdownNm)
                        }
                    }
                    DispatchQueue.main.async(execute: {
                        // perform on main
                        print(self.petKindListDic.count);
                        let values = Array(self.petKindListDic.values)
                        ActionSheetStringPicker.show(withTitle: "세부 품종 선택", rows:
                            values
                            , initialSelection: 0, doneBlock: {
                                picker, index, value in
                                
                                print("values = \(value.debugDescription)")
                                print("indexes = \(index)")
                                print("picker = \(picker.debugDescription)")
                                sender.setTitle(value as? String, for: UIControlState.normal)
                                for e in self.petKindListDic{
                                    if (sender.currentTitle!.isEqual(e.value)) {
                                        self.selectedKind = e.key
                                    }
                                }
                                self.btnDetailKindRemove.isEnabled = true
                                return
                        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
                    });
                }
            }
        }
        task.resume()

    }
    @IBAction func startDatePickerTapped(_ sender: UILabel) {
        var title: String
        print(sender.debugDescription)
        
        title = "검색시작일"
        
        DatePickerDialog().show(title, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if(date != nil){
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let formattedDate = dateFormatter.string(from: date!)
                
                self.srchStartDy.text = "\(formattedDate)"
                self.selectedSrchStartDy = formattedDate.replacingOccurrences(of: "-", with: "")
                self.btnSrchStartDtRemove.isEnabled = true
            }
        }
        
    }
    @IBAction func endDatePickerTapped(_ sender: UILabel) {
        var title: String
        print(sender.debugDescription)
 
            title = "검색종료일"
 
        DatePickerDialog().show(title, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if(date != nil){
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let formattedDate = dateFormatter.string(from: date!)
                
                self.srchEndDy.text = "\(formattedDate)"
                self.selectedSrchEndDy = formattedDate.replacingOccurrences(of: "-", with: "")
                self.btnSrchEndDtRemove.isEnabled = true
            }
        }
      
    }
    @IBAction func removeSidoBtn(_ sender: UIButton) {
        print("Called removeSidoBtn")
        btnCitySelector.setTitle("시도 선택 (전체)", for: UIControlState.normal)
        self.selectedUprCd = ""
        btnGuSelector.setTitle("구 선택 (전체)", for: UIControlState.normal)
        self.selectedOrgCd = ""
        sender.isEnabled = false
        btnGuRemove.isEnabled = false
    }
    @IBAction func removeGuBtn(_ sender: UIButton) {
        btnGuSelector.setTitle("구 선택 (전체)", for: UIControlState.normal)
        self.selectedOrgCd = ""
        sender.isEnabled = false
    }
    @IBAction func removeKindBtn(_ sender: UIButton) {
        btnPetTypeSelector.setTitle("축종 (전체)", for: UIControlState.normal)
        self.selectedUpkind = ""
        btnPetDetailTypeSelector.setTitle("세부축종 (전체)", for: UIControlState.normal)
        self.selectedKind = ""
        sender.isEnabled = false
        btnDetailKindRemove.isEnabled = false
    }
    @IBAction func removeDetailKindBtn(_ sender: UIButton) {
        btnPetDetailTypeSelector.setTitle("세부축종 (전체)", for: UIControlState.normal)
        self.selectedKind = ""
        sender.isEnabled = false
    }
    @IBAction func removeSrchStartBtn(_ sender: UIButton) {
        self.srchStartDy.text = ""
        self.selectedSrchStartDy = ""
        self.btnSrchStartDtRemove.isEnabled = false
    }
    @IBAction func removeSrchEndBtn(_ sender: UIButton) {
        self.srchEndDy.text = ""
        self.selectedSrchEndDy = ""
        self.btnSrchEndDtRemove.isEnabled = false
    }
    @IBAction func goSearch(_ sender: UIButton) {
        
        self.tableView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
        self.indicatorTableView.startAnimating()
        if self.btnCitySelector.currentTitle! == "시도선택 (전체)" {
            self.view.showToast("시도를 먼저 선택해주세요", position: .bottom, popTime: 3, dismissOnTap: false)
            return
        }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: "http://127.0.0.1:8080/abandonpet/getAbandonPetList?bgnde="+self.selectedSrchStartDy+"&endde="+self.selectedSrchEndDy+"&upkind="+self.selectedUpkind+"&kind="+self.selectedKind+"&uprCd="+self.selectedUprCd+"&orgCd="+self.selectedOrgCd)!
        let task = session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                UIUtils.popupToast(message: "네트워크 장애가 발생하였습니다", view: self.view)
            } else {
                let dataString =  String(data: data!, encoding: String.Encoding.utf8)
                print(dataString!)
                let xml = dataString!
                let xmlDom = SWXMLHash.config({config in config.shouldProcessLazily = true}).parse(xml as String)
                
                let items = xmlDom["body"]["items"]["item"].all
                self.abandonpetinfos.removeAll()
                for item in items{
                    
                    let age = item["age"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let careAddr = item["careAddr"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let careNm = item["careNm"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let careTel = item["careTel"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let chargeNm = item["chargeNm"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let colorCd = item["colorCd"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let desertionNo = item["desertionNo"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let filename = item["filename"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let happenDt = item["happenDt"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let happenPlace = item["happenPlace"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let kindCd = item["kindCd"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let neuterYn = item["neuterYn"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let noticeEdt = item["noticeEdt"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let noticeSdt = item["noticeSdt"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let officetel = item["officetel"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let orgNm = item["orgNm"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let popfile = item["popfile"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let processState = item["processState"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let sexCd = item["sexCd"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let specialMark = item["specialMark"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let weight = item["weight"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let numOfRows = Int((xmlDom["body"]["numOfRows"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines))!)
                    let pageNo = Int((xmlDom["body"]["pageNo"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines))!)
                    let totalCount = Int((xmlDom["body"]["totalCount"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines))!)
                    let abandonpetinfo = AbandonPetInfo(age: age!, careAddr: careAddr!, careNm: careNm!, careTel: careTel!, chargeNm: chargeNm!, colorCd: colorCd!, desertionNo: desertionNo!, filename: filename!, happenDt: happenDt!, happenPlace: happenPlace!, kindCd: kindCd!, neuterYn: neuterYn!, noticeEdt: noticeEdt!, noticeSdt: noticeSdt!, officetel: officetel!, orgNm: orgNm!, popfile: popfile!, processState: processState!, sexCd: sexCd!, specialMark: specialMark!, weight: weight!, numOfRows: numOfRows!, pageNo: pageNo!, totalCount: totalCount!)
                    self.abandonpetinfos.append(abandonpetinfo)
                }
                
                //self.tableView.beginUpdates()
                
                //self.tableView.insertRows(at: 0, with: UITableViewRowAnimationRight)
                //self.tableView.deleteRows(at: self.abandonpetinfos.count, with: UITableViewRowAnimationFade)
    
                
                //print(self.abandonpetinfos.count)
                //print(self.abandonpetinfos[0].age)
                DispatchQueue.main.async{
                    //self.tableView.reloadData()
                    let range = NSMakeRange(0, self.tableView.numberOfSections)
                    let sections = NSIndexSet(indexesIn: range)
                    self.tableView.reloadSections(sections as IndexSet, with: .automatic)
                    self.indicatorTableView.stopAnimating()
                }
                
                //print(json)
                //let json = try!JSONSerialization.jsonObject(with: data!, options: .mutableLeaves, .allowFragments) as? [[String:Any]]
                //siguListArray = json
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
//                if let parseJSON = json {
//                    for e in parseJSON{
//                        if let orgCd = e["orgCd"] as? String{
//                            let orgdownNm = e["orgdownNm"] as? String!
//                            self.siguListDic[orgCd] = orgdownNm
//                            //print(orgdownNm)
//                        }
//                    }
//                    DispatchQueue.main.async(execute: {
//                        // perform on main
//                        print(self.siguListDic.count);
//                        let values = Array(self.siguListDic.values)
//                        ActionSheetStringPicker.show(withTitle: "구 선택", rows:
//                            values
//                            , initialSelection: 0, doneBlock: {
//                                picker, index, value in
//                                
//                                print("values = \(value.debugDescription)")
//                                print("indexes = \(index)")
//                                print("picker = \(picker.debugDescription)")
//                                self.lblGuSelect.text = value as? String
//                                for e in self.siguListDic{
//                                    if (self.lblGuSelect.text?.isEqual(e.value))! {
//                                        self.selectedOrgCd = e.key
//                                    }
//                                }
//                                return
//                        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
//                    });
//                }
            }
        }
        task.resume()
        
        
        
    }
    func goSearchAdd(pageNo: Int) {
        self.indicatorTableView.startAnimating()
        if self.btnCitySelector.currentTitle! == "시도선택 (전체)" {
            self.view.showToast("시도를 먼저 선택해주세요", position: .bottom, popTime: 3, dismissOnTap: false)
            return
        }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: "http://127.0.0.1:8080/abandonpet/getAbandonPetList?bgnde="+self.selectedSrchStartDy+"&endde="+self.selectedSrchEndDy+"&upkind="+self.selectedUpkind+"&kind="+self.selectedKind+"&uprCd="+self.selectedUprCd+"&orgCd="+self.selectedOrgCd+"&pageNo="+String(pageNo))!
        let task = session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                UIUtils.popupToast(message: "네트워크 장애가 발생하였습니다", view: self.view)
            } else {
                let dataString =  String(data: data!, encoding: String.Encoding.utf8)
                print(dataString!)
                let xml = dataString!
                let xmlDom = SWXMLHash.config({config in config.shouldProcessLazily = true}).parse(xml as String)
                
                let items = xmlDom["body"]["items"]["item"].all
                //self.abandonpetinfos.removeAll()
                for item in items{
                    
                    let age = item["age"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let careAddr = item["careAddr"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let careNm = item["careNm"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let careTel = item["careTel"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    var chargeNm = item["chargeNm"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    if chargeNm == nil {chargeNm = "" }
                    let colorCd = item["colorCd"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let desertionNo = item["desertionNo"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let filename = item["filename"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let happenDt = item["happenDt"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let happenPlace = item["happenPlace"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let kindCd = item["kindCd"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let neuterYn = item["neuterYn"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let noticeEdt = item["noticeEdt"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let noticeSdt = item["noticeSdt"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    var officetel = item["officetel"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    if officetel == nil {officetel = "" }
                    let orgNm = item["orgNm"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let popfile = item["popfile"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let processState = item["processState"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let sexCd = item["sexCd"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let specialMark = item["specialMark"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let weight = item["weight"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines)
                    let numOfRows = Int((xmlDom["body"]["numOfRows"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines))!)
                    let pageNo = Int((xmlDom["body"]["pageNo"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines))!)
                    let totalCount = Int((xmlDom["body"]["totalCount"].element?.text.trimmingCharacters(in: .whitespacesAndNewlines))!)
                    let abandonpetinfo = AbandonPetInfo(age: age!, careAddr: careAddr!, careNm: careNm!, careTel: careTel!, chargeNm: chargeNm!, colorCd: colorCd!, desertionNo: desertionNo!, filename: filename!, happenDt: happenDt!, happenPlace: happenPlace!, kindCd: kindCd!, neuterYn: neuterYn!, noticeEdt: noticeEdt!, noticeSdt: noticeSdt!, officetel: officetel!, orgNm: orgNm!, popfile: popfile!, processState: processState!, sexCd: sexCd!, specialMark: specialMark!, weight: weight!, numOfRows: numOfRows!, pageNo: pageNo!, totalCount: totalCount!)
                    self.abandonpetinfos.append(abandonpetinfo)
                }
                
                //self.tableView.beginUpdates()
                
                //self.tableView.insertRows(at: 0, with: UITableViewRowAnimationRight)
                //self.tableView.deleteRows(at: self.abandonpetinfos.count, with: UITableViewRowAnimationFade)
//                UIView.animate(duration: 2.0, animations: {
//                    self.view.layer.position.x = 200.0
//                })
                
                //print(self.abandonpetinfos.count)
                //print(self.abandonpetinfos[0].age)
                DispatchQueue.main.async{
                    self.tableView.reloadData()
                    self.indicatorTableView.stopAnimating()
//                    let sectionToReload = 1
//                    let indexSet: IndexSet = [sectionToReload]
//                    
//                    self.tableView.reloadSections(indexSet, with: .automatic)
                    
                }
                
                //print(json)
                //let json = try!JSONSerialization.jsonObject(with: data!, options: .mutableLeaves, .allowFragments) as? [[String:Any]]
                //siguListArray = json
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                //                if let parseJSON = json {
                //                    for e in parseJSON{
                //                        if let orgCd = e["orgCd"] as? String{
                //                            let orgdownNm = e["orgdownNm"] as? String!
                //                            self.siguListDic[orgCd] = orgdownNm
                //                            //print(orgdownNm)
                //                        }
                //                    }
                //                    DispatchQueue.main.async(execute: {
                //                        // perform on main
                //                        print(self.siguListDic.count);
                //                        let values = Array(self.siguListDic.values)
                //                        ActionSheetStringPicker.show(withTitle: "구 선택", rows:
                //                            values
                //                            , initialSelection: 0, doneBlock: {
                //                                picker, index, value in
                //
                //                                print("values = \(value.debugDescription)")
                //                                print("indexes = \(index)")
                //                                print("picker = \(picker.debugDescription)")
                //                                self.lblGuSelect.text = value as? String
                //                                for e in self.siguListDic{
                //                                    if (self.lblGuSelect.text?.isEqual(e.value))! {
                //                                        self.selectedOrgCd = e.key
                //                                    }
                //                                }
                //                                return
                //                        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
                //                    });
                //                }
            }
        }
        task.resume()
        
        
        
    }
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        print(segue.identifier);
//        if segue.identifier == "ShowAttractionDetails" {
//            let detailViewController = segue.destinationViewController as! AttractionDetailViewController
//            let myIndexPath = self.tableView.indexPathForSelectedRow!
//            let row = myIndexPath.row
//            detailViewController.webSite = webAddress[row]
//        }
    }
    // Segue를 통해 DetailViewController에 데이터 넘기기
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            print("showDetail Segue");
            let detailVC = segue.destination as! AbandonPetDetailViewController
            
            // 테이블뷰에서 선택한 indexPath.row
            let selectedPath = tableView.indexPathForSelectedRow
            detailVC.abandonPetInfo = abandonpetinfos[(selectedPath?.row)!]
        }
    }
}
extension FirstViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("카운트왔음"+self.abandonpetinfos.count.description)
        return self.abandonpetinfos.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("Delegate왔음",indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath) as! MyCell
        let abandonpet = abandonpetinfos[indexPath.row]
        print(abandonpet.kindCd)
        print(abandonpet.specialMark)
        print(abandonpet.filename)
        cell.lblKindCd.text = abandonpet.kindCd
        cell.lblSpecialMark.text = "특이점 : ".appending(abandonpet.specialMark)
        cell.imgImageView.contentMode = .redraw
        cell.imgImageView.downloadedFrom(link: abandonpet.filename, contentMode: UIViewContentMode.scaleToFill)
        cell.lblCareNm.text = "보호소 : ".appending(abandonpet.careNm).appending(" / ").appending(abandonpet.careTel)
        cell.lblSexCd.text = abandonpet.sexCd
        //cell.lblCareTel.text = abandonpet.careTel
        cell.lblHappenDt.text = "발견일시 : ".appending(abandonpet.happenDt)
        cell.lblHappenPlace.text = "발견장소 : ".appending(abandonpet.happenPlace)
        cell.lblProcessState.text = abandonpet.processState
        cell.lblCareAddr.text = abandonpet.careAddr
        return cell
   //     return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row != 0 && indexPath.row == abandonpetinfos.count-1{
            print("WILL DISPLAY CALL",indexPath.row);
            if indexPath.row+1 >= abandonpetinfos[0].totalCount {
                
            }else{
                goSearchAdd(pageNo: (indexPath.row % 29)+2)
            }
            
            
        }
    }
    
   
}
extension FirstViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //self.performSegue(withIdentifier: "showDetail", sender: self);
    }

}


