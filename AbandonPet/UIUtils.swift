//
//  UIUtils.swift
//  AbandonPet
//
//  Created by Htrucci on 2017. 11. 15..
//  Copyright © 2017년 Htrucci. All rights reserved.
//

import Foundation
import UIKit

public class UIUtils {
   public static func popupToast(message: String, view: UIView){
        DispatchQueue.main.async(execute: {
        view.showToast(message, position: .bottom, popTime: 3, dismissOnTap: false)
        })
    }
    
}
