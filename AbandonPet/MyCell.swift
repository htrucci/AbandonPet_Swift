//
//  MyCell.swift
//  AbandonPet
//
//  Created by Htrucci on 2017. 7. 30..
//  Copyright © 2017년 Htrucci. All rights reserved.
//

import Foundation
import UIKit

class MyCell: UITableViewCell{
    
    @IBOutlet weak var lblKindCd: UILabel!
    @IBOutlet weak var imgImageView: UIImageView!
    @IBOutlet weak var lblCareNm: UILabel!
    @IBOutlet weak var lblSexCd: UILabel!
    @IBOutlet weak var lblSpecialMark: UILabel!
    @IBOutlet weak var lblHappenPlace: UILabel!
    @IBOutlet weak var lblCareTel: UILabel!
    @IBOutlet weak var lblHappenDt: UILabel!
    @IBOutlet weak var lblProcessState: UILabel!
    @IBOutlet weak var lblCareAddr: UILabel!
}



