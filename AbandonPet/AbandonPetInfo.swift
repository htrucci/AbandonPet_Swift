//
//  AbandonPetInfo.swift
//  AbandonPet
//
//  Created by Htrucci on 2017. 7. 30..
//  Copyright © 2017년 Htrucci. All rights reserved.
//

import Foundation

struct AbandonPetInfo {
    let age:String
    let careAddr:String
    let careNm:String
    let careTel:String
    let chargeNm:String
    let colorCd:String
    let desertionNo:String
    let filename:String
    let happenDt:String
    let happenPlace:String
    let kindCd:String
    let neuterYn:String
    let noticeEdt:String
    let noticeSdt:String
    let officetel:String
    let orgNm:String
    let popfile:String
    let processState:String
    let sexCd:String
    let specialMark:String
    let weight:String
    let numOfRows:Int
    let pageNo:Int
    let totalCount:Int
}
