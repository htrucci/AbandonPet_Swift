//
//  AbandonPetDetailViewController.swift
//  AbandonPet
//
//  Created by Htrucci on 2017. 8. 4..
//  Copyright © 2017년 Htrucci. All rights reserved.
//

import Foundation
import UIKit


class AbandonPetDetailViewController: UIViewController{
    @IBOutlet weak var popImage: UIImageView!
    @IBOutlet weak var lblKindCd: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblDestinationNo: UILabel!
    @IBOutlet weak var lblCareNm: UILabel!
    @IBOutlet weak var lblChargeNm: UILabel!
    @IBOutlet weak var lblColorCd: UILabel!
    @IBOutlet weak var lblCareTel: UILabel!
    @IBOutlet weak var lblCareAddr: UILabel!
    @IBOutlet weak var lblHappenPlace: UILabel!
    @IBOutlet weak var lblNeuterYn: UILabel!
    @IBOutlet weak var lblNoticeEdt: UILabel!
    @IBOutlet weak var lblHappenDt: UILabel!
    @IBOutlet weak var lblNoticeSdt: UILabel!
    @IBOutlet weak var lblOfficeTel: UILabel!
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblSpecialMark: UILabel!
    @IBOutlet weak var lblSexCd: UILabel!
    @IBOutlet weak var lblOrgNm: UILabel!
    @IBOutlet weak var lblProcessState: UILabel!
    var abandonPetInfo : AbandonPetInfo?
    override func viewDidLoad() {
        super.viewDidLoad()
        popImage.contentMode = .center
        popImage.downloadedFrom(link: (abandonPetInfo?.popfile)!, contentMode : .scaleAspectFill)
        lblKindCd.text = abandonPetInfo?.kindCd
        lblAge.text = abandonPetInfo?.age
        lblDestinationNo.text = abandonPetInfo?.desertionNo
        lblCareNm.text = abandonPetInfo?.careNm
        lblChargeNm.text = abandonPetInfo?.chargeNm
        lblColorCd.text = abandonPetInfo?.colorCd
        lblCareTel.text = abandonPetInfo?.careTel
        lblCareAddr.text = abandonPetInfo?.careAddr
        lblHappenPlace.text = abandonPetInfo?.happenPlace
        lblNeuterYn.textAlignment = NSTextAlignment.center
        lblNeuterYn.text = abandonPetInfo?.neuterYn
        lblNoticeEdt.text = abandonPetInfo?.noticeEdt
        lblHappenDt.text = abandonPetInfo?.happenDt
        lblNoticeSdt.text = abandonPetInfo?.noticeSdt
        lblWeight.text = abandonPetInfo?.weight
        lblSpecialMark.text = abandonPetInfo?.specialMark
        lblSexCd.text = abandonPetInfo?.sexCd
        lblOrgNm.text = abandonPetInfo?.orgNm
        lblProcessState.text = abandonPetInfo?.processState
        
    }
}
