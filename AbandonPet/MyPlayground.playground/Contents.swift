//: Playground - noun: a place where people can play

import UIKit
import SWXMLHash

let xmlWithNamespace = "<body><items><item><age>2016(년생)</age><careAddr>경기도 양주시</careAddr></item></items></body>"

var xml = SWXMLHash.parse(xmlWithNamespace)
let count = xml["body"]["items"]["item"].all.count
xml["body"]["items"]["item"]["age"].element?.text
xml["body"]["items"]["item"]["careAddr"].element?.text