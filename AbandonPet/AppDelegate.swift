		//
//  AppDelegate.swift
//  AbandonPet
//
//  Created by Htrucci on 2017. 7. 10..
//  Copyright © 2017년 Htrucci. All rights reserved.
//

import UIKit
import UserNotifications
import EasyAnimation
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate{

    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        EasyAnimation.enable()
        // Override point for customization after application launch.
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
        }
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound,.badge]){
            (granted,error) in
            if granted{
                DispatchQueue.main.async{
                application.registerForRemoteNotifications()
                }
            } else {
                print("User Notification permission denied: \(error?.localizedDescription)")
            }
            
        }

        return true
    }
     var fvc = FourthViewController()
    var tabBarController: UITabBarController?
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("실행중일때 Push왔음",userInfo["aps"])
        if var alertDict = userInfo["aps"] as? Dictionary<String, AnyObject> {
            print("!")
            var url = alertDict["url"] as! String
            print(url)
            self.tabBarController!.selectedIndex = 3
            fvc.apnsInit(url)

            print("@")
            
            print(url)
        }
        //application.applicationIconBadgeNumber = 0;
        if (application.applicationState == UIApplicationState.active) {
            print("active")
//            let content = UNMutableNotificationContent()
//            content.title = "Intro to Notifications"
//            content.subtitle = "Lets code,Talk is cheap"
//            content.body = "Sample code from WWDC"
//            content.sound = UNNotificationSound.default()
//            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5.0, repeats: false)
//            let request = UNNotificationRequest(identifier:"AbandonPet", content: content, trigger: trigger)
            
//            UNUserNotificationCenter.current().delegate = self
            pushNotification2()
        }else{
            print("not active")
        }
    }
    // This method will be called when app received push notifications in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        print("foreground")
        completionHandler([.alert, .badge, .sound])
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //TODO: Add code here later to deal with tokens.
        print("Successful registration. Token is:")
        print(tokenString(deviceToken))
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for remote notifications: \(error.localizedDescription)")
    }
    func tokenString(_ deviceToken:Data) -> String{
        //code to make a token string
        let bytes = [UInt8](deviceToken)
        var token = ""
        for byte in bytes{
            token += String(format: "%02x",byte)
        }
        return token
    }
    private func pushNotification2() {
        
        let content = UNMutableNotificationContent()
        content.title = "Intro to Notifications"
        content.subtitle = "Lets code,Talk is cheap"
        content.body = "Sample code from WWDC"
        content.sound = UNNotificationSound.default()
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5.0, repeats: false)
        let request = UNNotificationRequest(identifier:"AbandonPet", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().delegate = self
        
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
//        let urlScheme = url.scheme //[URL_scheme]
//        let host = url.host //red
        print("appdelegate:"+url.scheme!)
        print("appdelegate:"+url.host!)
        
        return true
    }
    
    
}

